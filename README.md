## Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

## Відповіді на питання

1. Особливий інтерфейс, який показує, як ваші HTML та XML документи читаються браузером.
2.  innerText - те ж, що і innerHTML, проте не може містити HTML теги;
3. По ID,по класу,по тегу (getElementById(),getElementsByTagName(),getElementsByClassName())
але краще звертатись за допомогою querySelector() або querySelectorAll().